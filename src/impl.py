import server
import hashlib

digitsWanted = 3

def sha1(x):
    return hashlib.sha1(x).hexdigest()

class MainHandler:
    def __init__(self):
        pass
    def getWorkUnit(self):
        return server.WorkUnit(server.getRandomData(32))
    def handleWorkUnit(self, workunit):
        if sha1(workunit.response)[:digitsWanted] == "0" * digitsWanted:
            print "And then there was much rejoycing: %s" % workunit.response
        else:
            print "Invalid sha1! %s" % sha1(workunit.response)

server.setWorkUnitImpl(MainHandler())
server.run()